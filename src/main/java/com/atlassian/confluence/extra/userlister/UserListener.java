package com.atlassian.confluence.extra.userlister;

import com.atlassian.confluence.event.events.security.LoginEvent;
import com.atlassian.confluence.event.events.security.LogoutEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserListener implements InitializingBean, DisposableBean {

    private final EventPublisher eventPublisher;
    private final UserListManager userListManager;

    @Autowired
    public UserListener(final @ComponentImport EventPublisher eventPublisher, final UserListManager userListManager) {
        this.eventPublisher = eventPublisher;
        this.userListManager = userListManager;
    }

    @EventListener
    public void handleLoginEvent(final LoginEvent loginEvent) {
        userListManager.registerLoggedInUser(loginEvent.getUsername(), loginEvent.getSessionId());
    }

    @EventListener
    public void handleLogoutEvent(final LogoutEvent logoutEvent) {
        userListManager.unregisterLoggedInUser(logoutEvent.getUsername(), logoutEvent.getSessionId());
    }

    @Override
    public void afterPropertiesSet() {
        eventPublisher.register(this);
    }

    @Override
    public void destroy() {
        eventPublisher.unregister(this);
    }
}
