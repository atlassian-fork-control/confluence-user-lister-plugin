package com.atlassian.confluence.extra.userlister;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext.GLOBAL_CONTEXT;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Component
public class DefaultUserListManager implements UserListManager {

    private final BandanaManager bandanaManager;
    private final CacheManager cacheManager;

    @Autowired
    public DefaultUserListManager(
            final @ComponentImport BandanaManager bandanaManager,
            final @ComponentImport CacheManager cacheManager) {
        this.bandanaManager = bandanaManager;
        this.cacheManager = cacheManager;
    }

    public Set<String> getGroupBlackList() {
        @SuppressWarnings("unchecked") final Set<String> blackList = (Set<String>) bandanaManager.getValue(GLOBAL_CONTEXT, BANDANA_KEY_BLACK_LIST);
        return null == blackList ? Collections.emptySet() : blackList;
    }

    public void saveGroupBlackList(Set<String> deniedGroups) {
        bandanaManager.setValue(GLOBAL_CONTEXT, BANDANA_KEY_BLACK_LIST, deniedGroups);
    }

    public boolean isGroupPermitted(String groupName) {
        return isBlank(groupName) || !getGroupBlackList().contains(groupName);
    }

    private Cache<String, Set<String>> getLoggedInUsersCache() {
        return cacheManager.getCache(getClass().getName());
    }

    public Set<String> getLoggedInUsers() {
        return new HashSet<>(getLoggedInUsersCache().getKeys());
    }

    private Set<String> getLoggedInUserSessionIds(String userName) {
        Set<String> sessionIds = getLoggedInUsersCache().get(userName);
        return null == sessionIds
                ? new HashSet<>()
                : sessionIds;
    }

    private void cacheLoggedInUser(String userName, Set<String> sessionIds) {
        getLoggedInUsersCache().put(userName, sessionIds);
    }

    public void registerLoggedInUser(String userName, String sessionId) {
        Set<String> sessionIds = getLoggedInUserSessionIds(userName);

        sessionIds.add(sessionId);
        cacheLoggedInUser(userName, sessionIds);
    }

    public void unregisterLoggedInUser(String userName, String sessionId) {
        Set<String> sessionIds = getLoggedInUserSessionIds(userName);

        sessionIds.remove(sessionId);
        if (sessionIds.isEmpty()) {
            getLoggedInUsersCache().remove(userName);
        } else {
            cacheLoggedInUser(userName, sessionIds);
        }
    }
}
