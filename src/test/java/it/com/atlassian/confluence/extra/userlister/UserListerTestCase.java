package it.com.atlassian.confluence.extra.userlister;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.GroupHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import com.atlassian.confluence.plugin.functest.helper.UserHelper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

public class UserListerTestCase extends AbstractConfluencePluginWebTestCase {
    private String testSpaceKey;

    protected void setUp() throws Exception {
        super.setUp();
        createTestSpace();
    }

    private void createTestSpace() {
        final SpaceHelper spaceHelper = getSpaceHelper();

        spaceHelper.setKey("TST");
        spaceHelper.setName("Test Space");

        assertTrue(spaceHelper.create());

        testSpaceKey = spaceHelper.getKey();
    }

    public void testEmptyGroupMessageDisplayedWhenEmptyGroupSpecified() {
        final PageHelper pageHelper = getPageHelper();
        final GroupHelper groupHelper = getGroupHelper();

        groupHelper.setName("empty-group");
        assertTrue(groupHelper.create());

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testEmptyGroupMessageDisplayedWhenEmptyGroupSpecified");
        pageHelper.setContent("{userlister:groups=" + groupHelper.getName() + "}");

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTextPresent("No results were found for groups : " + groupHelper.getName());
    }

    public void testUserRepeatedInGroupListsIfHeIsMemberOfThoseGroups() {
        final PageHelper pageHelper = getPageHelper();
        final UserHelper userHelper = getUserHelper();

        userHelper.setName("jdoe");
        userHelper.setFullName("John Doe");
        userHelper.setEmailAddress(userHelper.getName() + "@confluence");
        userHelper.setPassword(userHelper.getName());
        userHelper.setGroups(Arrays.asList("confluence-users"));

        assertTrue(userHelper.create());

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testEmptyGroupMessageDisplayedWhenEmptyGroupSpecified");
        pageHelper.setContent("{userlister:groups=confluence-users,confluence-administrators}");

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        /* Listing of group confluence-users should return admin and jdoe. */
        assertTableEquals(
                "userlister_group_" + StringEscapeUtils.escapeHtml("confluence-users"),
                new String[][]{
                        new String[]{"Group: confluence-users"},
                        new String[]{"Admin (admin)\n admin@confluence"},
                        new String[]{userHelper.getFullName() + " (" + userHelper.getName() + ")\n " + userHelper.getEmailAddress()}
                }
        );

        /* Listing of group confluence-administrators should return only admin */
        assertTableEquals(
                "userlister_group_" + StringEscapeUtils.escapeHtml("confluence-administrators"),
                new String[][]{
                        new String[]{"Group: confluence-administrators"},
                        new String[]{"Admin (admin)\n admin@confluence"}
                }
        );
    }

    public void testBlacklistedGroupsDroppedFromListing() {
        final PageHelper pageHelper = getPageHelper();

        try {
            gotoPageWithEscalatedPrivileges("/admin/userlister/configure.action");
            setWorkingForm("configure-userlister-blacklist-form");
            setTextField("blackListEntries", "confluence-administrators"); /* Blacklist confluence-admin */
            submit("save");
        } finally {
            dropEscalatedPrivileges();
        }

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testEmptyGroupMessageDisplayedWhenEmptyGroupSpecified");
        pageHelper.setContent("{userlister:groups=*}");

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        /* Listing of group confluence-users should return admin and jdoe. */
        assertTableEquals(
                "userlister_group_" + StringEscapeUtils.escapeHtml("confluence-users"),
                new String[][]{
                        new String[]{"Group: confluence-users"},
                        new String[]{"Admin (admin)\n admin@confluence"},
                }
        );

        /* Listing of group confluence-administrators should be filtered */
        assertTableNotPresent(
                "userlister_group_" + StringEscapeUtils.escapeHtml("confluence-administrators")
        );
        assertTextPresent("The output you're seeing is filtered. It contains blacklisted group(s) below.");
        assertElementPresent("userlister_denied_group_list"); /* This is the list of denied groups */
    }

    public void testBlacklistedGroupsDroppedFromListingAndWarningSuppressed() {
        final PageHelper pageHelper = getPageHelper();

        try {
            gotoPageWithEscalatedPrivileges("/admin/userlister/configure.action");
            setWorkingForm("configure-userlister-blacklist-form");
            setTextField("blackListEntries", "confluence-administrators"); /* Blacklist confluence-admin */
            submit("save");
        } finally {
            dropEscalatedPrivileges();
        }

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testEmptyGroupMessageDisplayedWhenEmptyGroupSpecified");
        pageHelper.setContent("{userlister:groups=*|showWarning=false}");

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        /* Listing of group confluence-users should return admin and jdoe. */
        assertTableEquals(
                "userlister_group_" + StringEscapeUtils.escapeHtml("confluence-users"),
                new String[][]{
                        new String[]{"Group: confluence-users"},
                        new String[]{"Admin (admin)\n admin@confluence"},
                }
        );

        /* Listing of group confluence-administrators should be filtered */
        assertTableNotPresent(
                "userlister_group_" + StringEscapeUtils.escapeHtml("confluence-administrators")
        );
        assertTextNotPresent("The output you're seeing is filtered. It contains blacklisted group(s) below.");
        assertElementNotPresent("userlister_denied_group_list"); /* This is the list of denied groups not present */
    }

    // CONF-16644
    public void testXSSinGroupParamWithNoUserInGroup() {
        final String groupName = "<script>alert('Vulnerable')</script>";
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testXSSinGroupParamWithNoUserInGroup");
        pageHelper.setContent("{userlister:groups=" + groupName + "}");

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        // group name should appear as normal text i.e. already escaped
        assertTextPresent("No results were found for groups : <script>alert('Vulnerable')</script>");
    }

    public void testXSSinGroupParamWithUsersInGroup() {
        final String groupName = "<script>alert('vulnerable')</script>";

        final GroupHelper groupHelper = getGroupHelper();
        groupHelper.setName(groupName);
        assertTrue(groupHelper.create());

        final UserHelper userHelper = getUserHelper();
        userHelper.setName("jdoe");
        userHelper.setFullName("John Doe");
        userHelper.setEmailAddress(userHelper.getName() + "@confluence");
        userHelper.setPassword(userHelper.getName());
        userHelper.setGroups(Arrays.asList(groupName));
        assertTrue(userHelper.create());

        final PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testXSSinGroupParamWithUsersInGroup");
        pageHelper.setContent("{userlister:groups=" + groupName + "}");
        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        // group name should appear as normal text i.e. already escaped
        assertTextPresent("Group: <script>alert('vulnerable')</script>");
    }

    public void testNullUserEmailNotShown() throws IOException {
        File testData = getClassPathResourceAsFile("USERLISTER-13-test-data.zip", ".zip");

        try {
            getConfluenceWebTester().restoreData(testData);

            createTestSpace();

            PageHelper pageHelper = getPageHelper();
            pageHelper.setSpaceKey(testSpaceKey);
            pageHelper.setTitle("testNullUserEmailNotShown");
            pageHelper.setContent("{userlister:groups=confluence-users}");
            assertTrue(pageHelper.create());

            gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

            assertTableEquals(
                    "userlister_group_" + StringEscapeUtils.escapeHtml("confluence-users"),
                    new String[][]{
                            new String[]{"Group: confluence-users"},
                            new String[]{"admin (admin)\n"},
                    }
            );
        } finally {
            assertTrue(testData.delete());
        }
    }

    private File getClassPathResourceAsFile(String classPathResource, String fileExtension)
            throws IOException {
        File testData = File.createTempFile("it.com.atlassian.confluence.extra.userlister", fileExtension);
        InputStream testDataInput = null;
        OutputStream testDataOutput = null;

        try {


            testDataInput = getClass().getClassLoader().getResourceAsStream(classPathResource);

            assertNotNull(testDataInput);
            testDataOutput = new FileOutputStream(testData);

            IOUtils.copy(testDataInput, testDataOutput);

            return testData;
        } finally {
            IOUtils.closeQuietly(testDataOutput);
            IOUtils.closeQuietly(testDataInput);
        }
    }
}
