package com.atlassian.confluence.extra.userlister;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.extra.userlister.model.ListedUser;
import com.atlassian.confluence.extra.userlister.model.UserList;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.sal.api.rdbms.ConnectionCallback;
import com.atlassian.sal.api.rdbms.TransactionalExecutor;
import com.atlassian.sal.api.rdbms.TransactionalExecutorFactory;
import com.atlassian.user.Group;
import com.atlassian.user.User;
import com.atlassian.user.impl.DefaultGroup;
import com.atlassian.user.impl.DefaultUser;
import com.atlassian.user.search.page.DefaultPager;
import com.atlassian.user.search.page.Pager;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.hamcrest.HamcrestArgumentMatcher;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserListerTestCase {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public TestRule restoreSystemProperties = new RestoreSystemProperties();

    @Mock
    private UserAccessor userAccessor;
    @Mock
    private UserListManager userListManager;
    @SuppressWarnings("unused")
    @Mock
    private LocaleManager localeManager;
    @Mock
    private I18NBeanFactory i18NBeanFactory;
    @Mock
    private I18NBean i18NBean;
    @Mock
    private VelocityHelperService velocityHelperService;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private TransactionalExecutorFactory transactionalExecutorFactory;
    @Mock
    private TransactionalExecutor transactionalExecutor;

    @Mock
    private ConfluenceUser testUser;

    @InjectMocks
    private UserLister userLister;

    private Map<String, String> macroParameters;
    private Page pageToRender;

    @Before
    public void setUp() {
        when(i18NBeanFactory.getI18NBean(any())).thenReturn(i18NBean);
        when(i18NBean.getText(anyString())).thenAnswer(returnsFirstArg());
        when(i18NBean.getText(anyString(), any(List.class))).thenAnswer(returnsFirstArg());

        when(permissionManager.hasPermission(
                any(),
                eq(Permission.VIEW),
                eq(PermissionManager.TARGET_PEOPLE_DIRECTORY)
        )).thenReturn(true);

        when(transactionalExecutorFactory.createExecutor(true, true)).thenReturn(transactionalExecutor);

        when(transactionalExecutor.execute(any())).thenAnswer(invocationOnMock -> {
            ConnectionCallback callback = (ConnectionCallback) invocationOnMock.getArguments()[0];
            return callback.execute(null);
        });

        macroParameters = new HashMap<>();
        pageToRender = new Page();
    }

    @Test
    public void testErrorMessageRenderedIfGroupNotSpecifiedInMacroParameters() throws MacroException {
        when(userListManager.getGroupBlackList()).thenReturn(emptySet());

        assertEquals(
                "userlister.no.groups.specified",
                userLister.execute(macroParameters, StringUtils.EMPTY, pageToRender.toPageContext())
        );
    }

    @Test
    public void testErrorMessageRenderedWhenThereAreTooManyUsers() throws MacroException {
        final String groupName = "confluence-users";
        final Group userGroup = new DefaultGroup(groupName);

        System.setProperty(UserLister.USER_LISTER_LIMIT_PROPERTY, "3");

        when(userAccessor.getGroup(groupName)).thenReturn(userGroup);
        when(userAccessor.getMemberNames(any())).thenReturn(new DefaultPager<>(asList("user1", "user2", "user3")));
        when(userAccessor.getUserByName(any())).thenReturn(testUser);

        macroParameters.put("groups", "confluence-users");

        assertEquals(
                "userlister.too.many.users",
                userLister.execute(macroParameters, StringUtils.EMPTY, pageToRender.toPageContext())
        );
    }

    @Test
    public void testErrorMessageRenderedWhenThereAreTooManyUsersInMultiGroups() throws MacroException {
        final String userGroupName = "confluence-users";
        final String adminGroupName = "confluence-admins";
        final Group userGroup = new DefaultGroup(userGroupName);
        final Group adminGroup = new DefaultGroup(adminGroupName);

        System.setProperty(UserLister.USER_LISTER_LIMIT_PROPERTY, "5");

        when(userAccessor.getGroup(userGroupName)).thenReturn(userGroup);
        when(userAccessor.getGroup(adminGroupName)).thenReturn(adminGroup);
        when(userAccessor.getMemberNames(userGroup)).thenReturn(new DefaultPager<>(asList("user1", "user2", "user3")));
        when(userAccessor.getMemberNames(adminGroup)).thenReturn(new DefaultPager<>(asList("user1", "user2", "user4")));

        when(userAccessor.getUserByName(any())).thenReturn(testUser);

        macroParameters.put("groups", "confluence-users, confluence-admins");

        assertEquals(
                "userlister.too.many.users",
                userLister.execute(macroParameters, StringUtils.EMPTY, pageToRender.toPageContext())
        );
    }

    @Test
    public void testUserDisplayedWithCorrectStatus() throws MacroException {
        final Group userGroup = new DefaultGroup("confluence-users");
        final ConfluenceUser admin = new ConfluenceUserImpl("admin", "", "");
        final ConfluenceUser user = new ConfluenceUserImpl("user1", "", "");

        when(userAccessor.getGroup(userGroup.getName())).thenReturn(userGroup);
        when(userAccessor.getMemberNames(any())).thenReturn(new DefaultPager<>(asList(user.getName(), admin.getName())));
        when(userAccessor.getUserByName(admin.getName())).thenReturn(admin);
        when(userAccessor.getUserByName(user.getName())).thenReturn(user);
        when(userListManager.getLoggedInUsers()).thenReturn(singleton(admin.getName()));

        macroParameters.put("groups", "confluence-users");
        userLister.execute(macroParameters, StringUtils.EMPTY, pageToRender.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(anyString(),
                doMatch("users in context map contains incorrect status", contextMap -> {
                            List<UserList> userLists = (List<UserList>) contextMap.get("userlists");
                            UserList userList = userLists.get(0);

                            ListedUser testAdmin = userList.getUsers().get(0);
                            ListedUser testUser = userList.getUsers().get(1);

                            return testAdmin.isLoggedIn() && !testUser.isLoggedIn();
                        }

                )
        );
    }

    @Test
    public void testErrorMessageRenderedIfWildcardSpecifiedAndBlacklistedGroupContainAsteriskAsOneEntry() throws MacroException {
        when(userListManager.getGroupBlackList()).thenReturn(singleton(UserList.ALL_GROUP_NAME));

        macroParameters.put("groups", UserList.ALL_GROUP_NAME);

        assertEquals(
                "userlister.group.name.list.contains.asterisk",
                userLister.execute(macroParameters, StringUtils.EMPTY, pageToRender.toPageContext())
        );
    }

    @Test
    public void testGroupsProperlyGroupedIntoDeniedAllowedAndEmptyCategories() throws MacroException {
        final Collection<String> blacklistedGroups = singletonList("confluence-users");
        final String confluenceAdminGroupName = "confluence-administrators";
        final Group confluenceAdminGroup = new DefaultGroup(confluenceAdminGroupName);
        final ConfluenceUser confluenceAdminMember = new ConfluenceUserImpl("admin", "", "");
        final String newGroupName = "new-group";
        final Group newGroup = new DefaultGroup(newGroupName);
        final String inactiveUsersOnlyGroupName = "inactive-group";
        final Group inactiveUsersGroup = new DefaultGroup(inactiveUsersOnlyGroupName);
        final User inactiveMember = new DefaultUser("inactive-member");

        when(userListManager.getGroupBlackList()).thenReturn(new HashSet<>(blacklistedGroups));


        /* Mock scenario where "admin" in group "confluence-administrators" is valid and active. */
        when(userAccessor.getGroup(confluenceAdminGroupName)).thenReturn(confluenceAdminGroup);
        when(userAccessor.getMemberNames(confluenceAdminGroup)).thenReturn(
                new DefaultPager<>(singletonList(confluenceAdminMember.getName()))
        );
        when(userAccessor.getUserByName(confluenceAdminMember.getName())).thenReturn(confluenceAdminMember);

        /* Mock scenario where "new-group" has no members. */
        when(userAccessor.getGroup(newGroupName)).thenReturn(newGroup);
        when(userAccessor.getMemberNames(newGroup)).thenReturn(
                new DefaultPager<>(emptyList())
        );

        /* Mock scenario where "inactive-member" in group "inactive-group" is valid but not active. */
        when(userAccessor.getGroup(inactiveUsersOnlyGroupName)).thenReturn(inactiveUsersGroup);
        when(userAccessor.getMemberNames(inactiveUsersGroup)).thenReturn(
                new DefaultPager<>(singletonList(inactiveMember.getName()))
        );
        when(userAccessor.isDeactivated(inactiveMember)).thenReturn(true);

        macroParameters.put("groups", ", ,,confluence-users, " + StringUtils.join(asList(confluenceAdminGroupName, newGroupName, inactiveUsersOnlyGroupName), ", "));

        userLister.execute(macroParameters, StringUtils.EMPTY, pageToRender.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(anyString(),
                doMatch("Not the context map expected", contextMap -> {
                            List<UserList> userLists = (List<UserList>) contextMap.get("userlists");
                            return new HashSet<>(blacklistedGroups).equals(contextMap.get("deniedGroups"))
                                    && StringUtils.equals("userlister.noresultsfoundforgroups", (String) contextMap.get("emptyGroups"))
                                    && null != userLists
                                    && 1 == userLists.size()
                                    && null != userLists.get(0)
                                    && StringUtils.equals(confluenceAdminGroupName, userLists.get(0).getGroup());
                        }
                )
        );
    }

    @Test
    public void testGroupsProperlyGroupedIntoDeniedAllowedAndEmptyCategoriesWhenWilcardSpecified() throws MacroException {
        final List<String> blacklistedGroups = singletonList("confluence-users");
        final String confluenceUsersGroupName = "confluence-users";
        final Group confluenceUsersGroup = new DefaultGroup(confluenceUsersGroupName);
        final String confluenceAdminGroupName = "confluence-administrators";
        final Group confluenceAdminGroup = new DefaultGroup(confluenceAdminGroupName);
        final ConfluenceUser confluenceAdminMember = new ConfluenceUserImpl("admin", "", "");
        final String newGroupName = "new-group";
        final Group newGroup = new DefaultGroup(newGroupName);
        final String inactiveUsersOnlyGroupName = "inactive-group";
        final Group inactiveUsersGroup = new DefaultGroup(inactiveUsersOnlyGroupName);
        final ConfluenceUser inactiveMember = new ConfluenceUserImpl("inactive-member", "", "");
        final List<Group> allGroups = asList(confluenceUsersGroup, confluenceAdminGroup, inactiveUsersGroup, newGroup);
        final Pager<Group> allGroupsPager = new DefaultPager<>(allGroups);

        when(userListManager.getGroupBlackList()).thenReturn(new HashSet<>(blacklistedGroups));

        /* Returns all accessible groups by the authenticated user */
        when(userAccessor.getGroups()).thenReturn(allGroupsPager);

        /* Mock scenario where "admin" in group "confluence-administrators" is valid and active. */
        when(userAccessor.getGroup(confluenceAdminGroupName)).thenReturn(confluenceAdminGroup);
        when(userAccessor.getMemberNames(confluenceAdminGroup)).thenReturn(
                new DefaultPager<>(singletonList(confluenceAdminMember.getName()))
        );
        when(userAccessor.getUserByName(confluenceAdminMember.getName())).thenReturn(confluenceAdminMember);

        /* Mock scenario where "new-group" has no members. */
        when(userAccessor.getGroup(newGroupName)).thenReturn(newGroup);
        when(userAccessor.getMemberNames(newGroup)).thenReturn(
                new DefaultPager<>(emptyList())
        );

        /* Mock scenario where "inactive-member" in group "inactive-group" is valid but not active. */
        when(userAccessor.getGroup(inactiveUsersOnlyGroupName)).thenReturn(inactiveUsersGroup);
        when(userAccessor.getMemberNames(inactiveUsersGroup)).thenReturn(
                new DefaultPager<>(singletonList(inactiveMember.getName()))
        );
        when(userAccessor.getUserByName(inactiveMember.getName())).thenReturn(inactiveMember);
        when(userAccessor.isDeactivated(inactiveMember)).thenReturn(true);

        macroParameters.put("groups", UserList.ALL_GROUP_NAME);

        userLister.execute(macroParameters, StringUtils.EMPTY, pageToRender.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(anyString(),
                doMatch("Not the context map expected", contextMap -> {
                            List<UserList> userLists = (List<UserList>) contextMap.get("userlists");

                            return new HashSet<>(blacklistedGroups).equals(contextMap.get("deniedGroups"))
                                    && StringUtils.equals("userlister.noresultsfoundforgroups", (String) contextMap.get("emptyGroups"))
                                    && null != userLists
                                    && 1 == userLists.size()
                                    && null != userLists.get(0)
                                    && StringUtils.equals(confluenceAdminGroupName, userLists.get(0).getGroup());
                        }
                )
        );
    }

    @Test
    public void testListModeIsAllWhenOnlineParameterNotSpecified() throws MacroException {
        macroParameters.put("groups", "confluence-users");
        userLister.execute(macroParameters, "", pageToRender.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(anyString(),
                doMatch("context map does not contain correct online parameters", contextMap ->
                        !contextMap.containsKey("online") && (Boolean) contextMap.get("allUserStatuses"))
        );
    }

    @Test
    public void testListModeIsOnlineWhenOnlineParameterIsSetToTrue() throws MacroException {
        macroParameters.put("groups", "confluence-users");
        macroParameters.put("online", "true");

        userLister.execute(macroParameters, "", pageToRender.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(anyString(),
                doMatch("online status in contextMap is not true", contextMap -> (Boolean) contextMap.get("online"))
        );
    }

    @Test
    public void testListModeIsOfflineWhenOnlineParameterIsNotSetToTrue() throws MacroException {
        macroParameters.put("groups", "confluence-users");
        macroParameters.put("online", "invalid");

        userLister.execute(macroParameters, "", pageToRender.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(anyString(),
                doMatch("online status in contextMap is not false", contextMap -> !((Boolean) contextMap.get("online")))
        );
    }

    @Test
    public void testMacroGeneratesBlockHtml() {
        assertEquals(TokenType.BLOCK, userLister.getTokenType(null, null, null));
    }

    @Test
    public void testMacroHasNoBody() {
        assertFalse(userLister.hasBody());
    }

    @Test
    public void testMacroRendersEverythingInBody() {
        assertEquals(RenderMode.NO_RENDER, userLister.getBodyRenderMode());
    }

    @Test
    public void testErrorMessageReturnedWhenUserDoesNotHavePermissionToViewUserProfile() throws MacroExecutionException {
        when(permissionManager.hasPermission(
                any(),
                eq(Permission.VIEW),
                eq(PermissionManager.TARGET_PEOPLE_DIRECTORY)
        )).thenReturn(false);
        assertEquals(
                "<div class=\"error\"><span class=\"error\">userlister.notpermitted.viewuserprofile</span> </div>",
                userLister.execute(new HashMap<>(), null, (ConversionContext) null)
        );
    }

    private <T> T doMatch(String msg, Function<T, Boolean> func) {
        return argThat(new HamcrestArgumentMatcher<>(new TypeSafeMatcher<T>() {
            public boolean matchesSafely(T object) {
                return func.apply(object);
            }

            public void describeTo(Description description) {
                description.appendText(msg);
            }
        }));
    }
}
